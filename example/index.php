<?php

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);

require_once(__DIR__ . '/../TinyPngCompress.php');

/**
 * EXAMPLES
 *
 * Uncomment the section what you want to try.
 */

try {
    // Compress single image BEGIN *************************************************************************************

//    $compress = new TinyPngCompress(array(
//        'apiKey' => 'YOUR API KEY',
//        'outputPath' => __DIR__ . '/output/'
//    ));
//    $compress->compress(__DIR__ . '/input/panda1.png');

    // Compress single image END ***************************************************************************************

    // Compress all images in input folder and its subfolders BEGIN ****************************************************

//    $compress = new TinyPngCompress(array(
//        'apiKey' => 'YOUR API KEY',
//        'inputPath' => __DIR__ . '/input/',
//        'outputPath' => __DIR__ . '/output/'
//    ));
//    $compress->compress();

    // Compress all images in input folder and its subfolders END ******************************************************

    // Compress all images in input folder and its subfolders (.png only) BEGIN ****************************************

//    $compress = new TinyPngCompress(array(
//        'apiKey' => 'YOUR API KEY',
//        'inputPath' => __DIR__ . '/input/',
//        'outputPath' => __DIR__ . '/output/',
//        'extensions' => array('png')
//    ));
//    $compress->compress();

    // Compress all images in input folder and its subfolders (.png only) END ******************************************

    // Compress and resize (generate thumbnail images too) BEGIN *******************************************************

//    $compress = new TinyPngCompress(array(
//        'apiKey' => 'YOUR API KEY',
//        'inputPath' => __DIR__ . '/input/',
//        'outputPath' => __DIR__ . '/output/',
//        'resize' => array(
//            array(
//                'postfix' => '-small',
//                'method' => 'scale',
//                'width' => 200
//            ),
//            array(
//                'postfix' => '-medium',
//                'method' => 'scale',
//                'width' => 600
//            )
//        )
//    ));
//    $compress->compress();

    // Compress and resize (generate thumbnail images too) END *********************************************************

} catch (Exception $ex) {
    echo $ex->getMessage();
}
