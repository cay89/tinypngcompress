<?php
/**
 * TinyPngCompress.php
 */

/**
 * TinyPngCompress - PHP class which supports mass image compression with the TinyPNG API.
 *
 * @author Csaba Keri <kericsaba89@gmail.com>
 * @version 2.0.0
 */
class TinyPngCompress {
    private $url = 'https://api.tinify.com/shrink';
    private $ssl = false;
    private $extensions = array('jpg', 'png');
    private $inputPath;
    private $outputPath;
    private $apiKey;
    private $curlOptions;
    /**
     * Resize data.
     *
     * Format:
     *
     * array(
     *      array(
     *          'outputPath' => [Resized image save path, if it's different from the original],
     *          'postfix' => [String after the original name],
     *          'method' => ['scale', 'fit', 'cover'],
     *          'width' => [Resized image width],
     *          'height' => [Resized image height]
     *      ),
     *      array(
     *          [Other size data HERE]
     *      )
     * )
     *
     * WARNING: When empty the 'outputPath' and the 'postfix' key too, it's overwriting the original image!
     *
     * @var array
     */
    private $resize = array();
    private $resizeOnly = false;

    /**
     * Perform the basic settings for the compression.
     *
     * @param array $settings Settings to compress.
     * @throws Exception
     */
    public function __construct(array $settings) {
        if(array_key_exists('apiKey', $settings) && !empty($settings['apiKey'])) {
            if(!empty($settings)) {
                foreach($settings as $key => $value) {
                    if(property_exists($this, $key)) {
                        $this->$key = $value;
                    }
                }
            }
            // Use default 'curl' settings
            if(empty($this->curlOptions)) {
                $this->curlOptions = array(
                    CURLOPT_URL => $this->url,
                    CURLOPT_USERPWD => 'api:' . $this->apiKey,
                    CURLOPT_BINARYTRANSFER => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_HEADER => true,
                    CURLOPT_SSL_VERIFYPEER => true
                );
                if($this->ssl === true) {
                    $this->curlOptions[CURLOPT_CAINFO] = __DIR__ . '/cacert.pem';
                }
            }
            // Use default output path
            if(empty($this->outputPath)) {
                $this->outputPath = __DIR__ . '/';
            }
        } else {
            throw new \Exception('API key not found.');
        }
    }

    /**
     * Running the compression process.
     *
     * @param null|string $path The path to the folder what contains the files (or a single file path) you want to compress.
     * @throws Exception
     */
    public function compress($path = null) {
        if(!is_null($path)) {
            $this->inputPath = $path;
        }

        if(!empty($this->inputPath)) {
            $files = array();
            if(is_dir($this->inputPath)) {
                self::searchFilesInPath($this->inputPath, $files, $this->extensions);
            } else {
                array_push($files, $this->inputPath);
            }

            if(!empty($files)) {
                foreach($files as $file) {
                    $this->curlCompress($file);
                }
            }
        } else {
            throw new \Exception('Input path cannot be empty!');
        }
    }

    /**
     * Compress with curl.
     *
     * @param string $file The file what you need to compress.
     * @throws Exception
     */
    private function curlCompress($file) {
        if(is_file($file)) {
            $fileName = basename($file);
            $outputFile = $this->outputPath . $fileName;
            $outputFileExtension = '.' . pathinfo($outputFile, PATHINFO_EXTENSION);
            $options = $this->curlOptions;
            $options[CURLOPT_POSTFIELDS] = file_get_contents($file);
            $request = curl_init();
            curl_setopt_array($request, $options);

            $response = curl_exec($request);

            $header_size = curl_getinfo($request, CURLINFO_HEADER_SIZE);
            $body = json_decode(substr($response, $header_size));
            if(curl_getinfo($request, CURLINFO_HTTP_CODE) === 201) {
                // Full size image
                if(!$this->resizeOnly) {
                    $request = curl_init();
                    $options = array(
                        CURLOPT_URL => $body->output->url,
                        CURLOPT_RETURNTRANSFER => $this->curlOptions[CURLOPT_RETURNTRANSFER],
                        CURLOPT_SSL_VERIFYPEER => $this->curlOptions[CURLOPT_SSL_VERIFYPEER]
                    );
                    curl_setopt_array($request, $options);
                    file_put_contents($outputFile, curl_exec($request));
                }

                // Resized images
                if(!empty($this->resize)) {
                    $options = array(
                        CURLOPT_URL => $body->output->url,
                        CURLOPT_RETURNTRANSFER => $this->curlOptions[CURLOPT_RETURNTRANSFER],
                        CURLOPT_SSL_VERIFYPEER => $this->curlOptions[CURLOPT_SSL_VERIFYPEER],
                        CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/json; charset=utf-8'
                        ),
                        CURLOPT_USERPWD => 'api:' . $this->apiKey
                    );
                    foreach($this->resize as $r) {
                        $request = curl_init();
                        $resizeOptions = array(
                            'resize' => array(
                                'method' => $r['method']
                            )
                        );
                        if(array_key_exists('width', $r) && !empty($r['width'])) {
                            $resizeOptions['resize']['width'] = $r['width'];
                        }
                        if(array_key_exists('height', $r) && !empty($r['height'])) {
                            $resizeOptions['resize']['height'] = $r['height'];
                        }
                        $options[CURLOPT_POSTFIELDS] = json_encode($resizeOptions);
                        curl_setopt_array($request, $options);
                        $resizedFileName = $fileName;
                        $resizedOutputFolder = $this->outputPath;
                        if(!empty($r['outputPath'])) {
                            $resizedOutputFolder = $r['outputPath'];
                            if(!is_dir($resizedOutputFolder)) {
                                mkdir($resizedOutputFolder);
                            }
                        }
                        if(!empty($r['postfix'])) {
                            $resizedFileName = str_replace($outputFileExtension, '', $fileName);
                            $resizedFileName .= $r['postfix'] . $outputFileExtension;
                        }
                        $resizedOutputFile = $resizedOutputFolder . $resizedFileName;
                        file_put_contents($resizedOutputFile, curl_exec($request));
                    }
                }
            } else {
                if(!empty($body) && property_exists($body, 'error') && property_exists($body, 'message')) {
                    throw new \Exception('Compression failed. ' . $body->error . ' ' . $body->message);
                }

                throw new \Exception('Compression failed. Curl error: ' . curl_error($request));
            }
        } else {
            throw new \Exception('The $file parameter (' . $file . ') is not a file.');
        }
    }

    /**
     * Searching for files in the specified folder and its subfolders recursively.
     *
     * @param string $path The folder's path
     * @param array $files The result array
     * @param array $extensions The extensions, which are examined
     */
    private static function searchFilesInPath($path, array &$files, array $extensions = array()) {
        $scandir = scandir($path);
        if(!empty($scandir)) {
            foreach ($scandir as $file) {
                if($file[0] != '.') {
                    if(is_dir($path . $file)) {
                        self::searchFilesInPath($path . $file . '/', $files, $extensions);
                    } else if(is_file($path . $file)) {
                        if(!empty($extensions)) {
                            $fileExtension = pathinfo($path . $file, PATHINFO_EXTENSION);
                            if(in_array($fileExtension, $extensions)) {
                                array_push($files, $path . $file);
                            }
                        } else {
                            array_push($files, $path . $file);
                        }
                    }
                }
            }
        }
    }
}
