[![SensioLabsInsight](https://insight.sensiolabs.com/projects/a83c1af5-1d51-42d7-8e8f-75df620e5f9c/big.png)](https://insight.sensiolabs.com/projects/a83c1af5-1d51-42d7-8e8f-75df620e5f9c)

# TinyPngCompress #

PHP class which supports mass image compression with the TinyPNG API.

## Usage ##

To be able to use this you will require an API key from [TinyPNG's developer page](https://tinypng.com/developers).

### Getting Started ###

Compress all images in input folder and its subfolders:

```
#!php
$compress = new TinyPngCompress(array(
    'apiKey' => 'YOUR API KEY',
    'inputPath' => __DIR__ . '/input/',
    'outputPath' => __DIR__ . '/output/'
));
$compress->compress();
```

More examples can be found in the 'example' folder.

### Basic settings ###
* **inputPath** - (*string*) The path to the folder what contains the files (or a single file path) you want to compress.
* **outputPath** - (*string*) The compressed files are saved to this location. - **default:** The folder that contains the class.
* **apiKey** - (*string*) An API key from [TinyPNG's developer page](https://tinypng.com/developers).

### Advanced settings ###
* **url** - (*string*) The TinyPNG API's url. - **default:** https://api.tinify.com/shrink (should not be modified)
* **mode** - (*string*) Method of file management: 'curl' or 'fopen'. - **default:** 'curl' **(Depricated: 2.0.0 supports curl only!)**
* **ssl** - (*bool*) Use SSL: true or false. - **default:** false (**No tested!**)
* **extensions** - (*array*) The extensions, which are examined. - **default:** array('jpg', 'png')
* **fopenOptions** - (*array*) Settings to compress in 'fopen' mode. - **defaults:** **(Depricated: 2.0.0 supports curl only!)**

```
#!php
array(
    'http' => array(
        'method' => 'POST',
        'header' => array(
            'Content-type: image/png',
            'Authorization: Basic ' . base64_encode('api:YOUR API KEY')
        )
    ),
    // only if the 'ssl' parameter is true!
    'ssl' => array(
        'cafile' => __DIR__ . '/cacert.pem',
        'verify_peer' => true
    )
);
```
* **curlOptions** - (*array*) Settings to compress in 'curl' mode. - **defaults:**

```
#!php
array(
    CURLOPT_URL => 'https://api.tinify.com/shrink',
    CURLOPT_USERPWD => 'api:YOUR API KEY',
    CURLOPT_BINARYTRANSFER => true,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HEADER => true,
    CURLOPT_SSL_VERIFYPEER => true,
    // only if the 'ssl' parameter is true!
    CURLOPT_CAINFO => __DIR__ . '/cacert.pem'
);
```
* **resize** - (*array*) Resize parameters. More info: https://tinypng.com/developers/reference

```
#!php
array(
    //...
    'resize' => array(
        array(
            'outputPath' => '' // Resized image save path, if it's different from the original,
            'postfix' => '', // String after the original name
            'method' => '', // 'scale', 'fit', 'cover'
            'width' => 0, // Resized image width
            'height' => 0 // Resized image height
        ),
        array(
            // Other size data HERE
        )
    )
)
```
* **resizeOnly** - (*bool*) True to no compress the original image.